package gob.hidalgo.curso.components.generales;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gob.hidalgo.curso.components.MensajesC;
import gob.hidalgo.curso.database.generales.ClienteOE;
import gob.hidalgo.curso.utils.Modelo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component("ClientesC")
public class ClientesC {
/*Logica de negocio que tenga que ver con la tabla clientes*/
	
	@Autowired
	private SqlSession sqlSession; /* declara instancia de base de datos.*/

	@Autowired
	private MensajesC mensajesC;
	
	public ClientesC() {
		super();
		// inyecta el log4j
		log.debug("Se crea el componente ClientesC");
	}
	
	public Modelo<ClienteOE> modelo(){
		List<ClienteOE> listado;
		listado = sqlSession.selectList("generales.clientes.listado");
		return new Modelo<ClienteOE>(listado);
		
	}
	
	public ClienteOE nuevo() {
		return new ClienteOE();
	}
	
	public boolean guardar(ClienteOE cliente) {
		
		if (cliente.getCelular().length()!=10) {
			mensajesC.mensajeError("Ingresa un N�mero v�lido (10 d�gitos)");
			return false;
		}
		
		if (cliente.getId() == null) {
			sqlSession.insert("generales.clientes.insertar", cliente);
			mensajesC.mensajeInfo("Cliente agregado exitosamente");
		}else {
			sqlSession.update("generales.clientes.actualizar", cliente);
			mensajesC.mensajeInfo("Cliente actualizado exitosamente");			
		}
		return true;
	}
	
	public boolean eliminar(ClienteOE cliente) {
		sqlSession.delete("generales.clientes.eliminar", cliente);
		mensajesC.mensajeInfo("Cliente eliminado exitosamente");
		return true;
	}
}
